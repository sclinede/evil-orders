bundle exec rake db:seed profile=rubyprof

echo 'rubyprof done'

bundle exec rake db:seed profile=stackprof mode=wall repeat=2 interval=100
stackprof tmp/wall_2_100 --graphviz | dot -Tpng -o tmp/graph_wall.png

bundle exec rake db:seed profile=stackprof mode=object repeat=1 interval=1
stackprof tmp/object_1_1 --graphviz | dot -Tpng -o tmp/graph_object.png
