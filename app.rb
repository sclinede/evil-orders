# frozen_string_literal: true

# rubocop:disable all

# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __FILE__)
require 'dotenv/load'

require 'bundler/setup' if File.exist?(ENV['BUNDLE_GEMFILE'])
require "rails"

require "action_controller/railtie"
require "action_view/railtie"
require "active_record/railtie"
require "pry-rails/railtie"

require "redis"
require "redis-rails"

require "slim-rails"
require "dry-initializer"
require "sidekiq"

require "pry-byebug"
require "better_errors"

require "rack-mini-profiler"
require "flamegraph"
require "memory_profiler"

require "sigdump/setup"
# GC::Profiler.enable

require "rbtrace"
require "ruby-prof"

require "scout_apm"

class EvilOrdersApp < Rails::Application
  secrets.secret_token    = "secret_token"
  secrets.secret_key_base = "secret_key_base"

  config.public_file_server.enabled = true

  config.consider_all_requests_local = Rails.env.development?
  config.cache_classes = Rails.env.production?
  config.action_controller.perform_caching = Rails.env.production?

  config.logger = Logger.new(STDOUT)
  config.log_level = ENV.fetch("LOG_LEVEL", :warn).to_sym

  config.eager_load = Rails.env.production?
  config.cache_store = :redis_store, ENV.fetch("REDIS_URL")

  initializer "routes" do
    Rails.application.routes.draw do
      root to: "home#index"

      scope "/api/v1" do
        resource :parcels, only: [:show, :create]
        resource :payment_update, only: [:create]
      end
    end
  end
end

# ========== CONTROLLERS ==========

class HomeController < ActionController::Base
  prepend_view_path Rails.root.join("views").to_s

  def index
    Rack::MiniProfiler.authorize_request if params[:profile] == "true"

    @orders = OrdersScope.new(params[:filters])
    @parcels = ParcelsScope.new(params[:filters])

    render layout: false
  end
end

class BaseController < ActionController::Metal
  include AbstractController::Callbacks
  include ActionController::StrongParameters

  before_action { self.content_type = "application/json" }
  around_action :catch_errors

  def catch_errors
    yield
  rescue ActiveRecord::RecordNotFound
    self.status = 404
    self.response_body = params.merge(error: "not_found").to_json
  rescue => error
    self.status = 422
    puts error.message
    puts error.backtrace
    self.response_body = params.merge(error: json_error(error)).to_json
  end

  private

  def json_error(error)
    {
      message: error.message,
      trace: error.backtrace,
    }
  end
end

class PaymentUpdatesController < BaseController
  def create
    with_ensure_payment_amount do
      PaymentProcessor.call(
        order,
        amount: raw_params["amount"],
        currency: raw_params["currency"]
      )

      self.status = 200
      self.response_body = {status: "Payment Accepted"}.to_json
    end
  end

  private

  def order
    @order ||= OrdersScope.new(raw_params).filtered.first!
  end

  def raw_params
    @raw_params ||= params.to_unsafe_h
  end

  def with_ensure_payment_amount
    catch :too_much_payment do
      yield
      return
    end

    self.status = 208
    self.response_body = {status: "Payment Already Applied"}.to_json
  end
end

class ParcelsController < BaseController
  def show
    self.response_body = Parcel.find_by(name: params[:name])
                               .attributes
                               .slice(*%w(name status origin destination))
                               .to_json
  end

  def create
     ParcelsCreator.call(
      order, params.to_unsafe_h, destination: params[:destination]
    )
    self.response_body = {order_id: order.id, status: "Created"}.to_json
  end

  private

  def order
    OrdersCreator.call(params.to_unsafe_h)
  end

  def order_attributes
    params.select(
      "products",
      "paid_amount",
      "shipping_cost",
      "contact_name",
      "contact_phone",
      "user_hash"
    ).merge(raw: params.to_json)
  end
end

# ========== MODELS ==========

class Order < ActiveRecord::Base
  PAYMENT_STATUSES = %w(products_not_paid shipping_not_paid success)

  validates :products, :paid_amount, :shipping_cost, presence: true
  validates :payment_status, inclusion: {in: PAYMENT_STATUSES}
  validate :payment_limits
  before_save { self.payment_status = evaluate_status }

  def payment_limits
    errors.add :paid_amount if paid_amount > total_cost
  end

  def total_cost
    products_cost + shipping_cost
  end

  def evaluate_status
    case paid_amount
    when 0...products_cost
      :products_not_paid
    when products_cost...total_cost
      :shipping_not_paid
    when total_cost
      :success
    end
  end
end

class Parcel < ActiveRecord::Base
  validates :name, :products, :destination, presence: true
  validates :products_count, numericality: {greater_than: 0}
  before_save { self.status = evaluate_status }

  STATUSES = %i(
    payment_not_complete
    waiting_for_shipment
    waiting_for_delivery
    delivered
    archived
  )

  def products_count
    products.to_a.size
  end

  def orders_statuses
    # Uniq Pluck vs Pluck Uniq
    Product.find(products).map { |p| p.order.payment_status }.uniq
  end

  def evaluate_status
    return :payment_not_complete if orders_statuses != [:success]
    return :waiting_for_shipment if shipped_at&.to_date.blank?
    return :waiting_for_delivery if delvired_at&.to_date.blank?
    :delivered
  end
end

class Product < ActiveRecord::Base
  STOREHOUSES = {
    "NYC" => "195 Montague St. \n Brooklyn, NY, 11201",
    "MSK" => "Chaplygina St., 6 \n Moscow, Russia, 105062",
    "SPB" => "Chapygina St., 6 \n Saint Petersburg, Russia, 197022",
    "TVT" => "Šetalište Iva Vizina br., 8 \n Tivat, Montenegro, 85320",
    "EKB" => "Šetalište Iva Vizina br., 8 \n Tivat, Montenegro, 85320"
  }

  belongs_to :order
  belongs_to :parcel

  validates :title, :cost, :quantity, presence: true
  validates :storehouse, inclusion: {in: STOREHOUSES.keys}
end

# ========== SERVICES ==========

class BaseServiceObject
  extend Dry::Initializer

  def self.call(*args)
    new(*args).call
  end
end

class PaymentProcessor < BaseServiceObject
  param :order
  option :amount, ->(a) { a.to_f }
  option :currency, ->(c) { c.upcase.to_sym }

  # in cents
  CURRENCY_RATES = {
    USD: 100,
    RUB: 1.7,
    EUR: 118.96,
  }

  delegate :transaction, to: :order

  def call
    transaction do
      order.lock!
      order.paid_amount += amount_in_cents
      order.save!
    end
  rescue => e
    throw :too_much_payment if order.errors[:paid_amount].present?
    raise e
  end

  private

  def amount_in_cents
    amount * CURRENCY_RATES[currency] .to_i
  end
end

class OrdersScope < BaseServiceObject
  param :filters, ->(f) { {paging: 0}.merge(f.to_h) }

  def filtered
    scopes.reduce(all) do |query, scope|
      query.merge(scope.call(self))
    end
  end

  def paid
    filtered.where(payment_status: :success)
  end

  def all
    Order.where("COALESCE(payment_status, '') <> 'archived'").order(id: :desc)
  end

  def order_id
    filters.fetch :order_id
  end

  def page_size
    size = filters.fetch(:paging).to_i
    return DEFAULT_PAGE_SIZE unless size.in?(0..MAX_PAGE_SIZE)
    size
  end

  def user_hash
    filters.fetch :user
  end

  private

  def scopes
    filters.keys.map { |filter| SCOPES[filter.to_sym] }.compact
  end

  SCOPES = {
    order_id: ->(scope) { Order.where(id: scope.order_id) },
    pagiпg: ->(scope) { Order.limit(scope.page_size) },
    user: ->(scope) { Order.where(user_hash: scope.user_hash) }
  }

  MAX_PAGE_SIZE = 500
  DEFAULT_PAGE_SIZE = 10
end

class ParcelsScope < BaseServiceObject
  param :filters, method(:Hash)

  def filtered
    scopes.reduce(all) { |query, scope| query.merge scope.call(filters) }
  end

  def ready
    filtered.where(status: :waiting_for_shipment)
  end

  def shipped
    filtered.where.not(shipped_at: nil)
  end

  def delivered
    filtered.where.not(delvired_at: nil)
  end

  def all
    Parcel.where("COALESCE(status, '') <> 'archived'").order(id: :desc)
  end
  alias to_a all

  def user_hash
    {user_hash: filters[:user]}
  end

  private

  def scopes
    filters.keys.map { |filter| SCOPES[filter] }.compact
  end

  SCOPES = {
    user: ->(scope) { all.where(scope.user_hash) }
  }

  MAX_PAGE_SIZE = 500
  DEFAULT_PAGE_SIZE = 10

  def page_size(size)
    return DEFAULT_PAGE_SIZE unless (size = size.to_i).in?(0..MAX_PAGE_SIZE)
    size
  end
end

class ProductsMapper < BaseServiceObject
  param :raw_products_data, method(:Array)

  def call
    raw_products_data.map do |raw_product|
      product = create_product(raw_product)
      increment_quantity(product)
      product.id
    end
  end

  private

  def create_product(raw_product_data)
    product = nil
    product_title = raw_product_data[:title]
    product = ::Product.find_or_create_by!(title: product_title) do |p|
      p.assign_attributes raw_product_data

      # convert to cents
      p.cost *= 100
    end
    product
  rescue ActiveRecord::RecordNotUnique
    retry
  end

  def increment_quantity(product)
    product.transaction do
      product.lock!
      product.quantity += 1
      product.save
    end
  end
end

class OrdersCreator < BaseServiceObject
  param :params, method(:Hash)

  ATTRIBUTES = %i(
    user_hash
    products
    products_cost
    shipping_cost
    paid_amount
    contact_name
    contact_phone
    raw
  )

  def call
    order = Order.create! from_attributes
    Product.where(id: product_ids).update_all(order_id: order.id)
    order
  end

  private

  def from_attributes
    base_attributes = params.merge(
      products: product_ids, raw: params,
      shipping_cost: shipping_cost_in_cents,
      products_cost: products_cost_in_cents
    ).slice(*ATTRIBUTES)

    base_attributes.merge(
      payment_status: Order.new(base_attributes).evaluate_status
    )
  end

  def products_cost_in_cents
    raw_products.map do |raw_product|
      raw_product[:cost].to_i * 100 * raw_product[:quantity].to_i
    end.sum
  end

  def shipping_cost_in_cents
    params[:shipping_cost].to_f * 100
  end

  def raw_products
    @raw_products ||= params.delete(:products)
  end

  def product_ids
    @product_ids ||= ProductsMapper.call(raw_products)
  end
end

class ParcelsCreator < BaseServiceObject
  param :order
  param :destination, method(:String)
  option :name, optional: true, as: :custom_name

  def call
    Product.where(id: order.products)
           .group_by(&:storehouse)
           .each do |storehouse, products|
      Parcel.create!(
        name: name,
        products: products.map(&:id),
        origin: Product::STOREHOUSES[storehouse],
        destination: destination
      ).tap do |parcel|
        link_products(products, parcel)
      end
    end
  end

  private

  def link_products(products, parcel)
    Product.transaction do
      products.each { |p| p.update(parcel_id: parcel.id) }
    end
  end

  def name
    custom_name || "Parcel ##{order.id}-#{Time.now.to_s(:number)}"
  end
end

Rails.application.initialize!
