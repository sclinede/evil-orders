-- example HTTP POST script which demonstrates setting the
-- HTTP method, body, and adding a header
--
-- sudo apt install luarocks
-- sudo luarocks install luasocket или luarocks install luasocket --local
socket = require("socket")

users_table = {
  "b3daa77b4c04a9551b8781d03191fe098f325e67",
  "a1881c06eec96db9901c7bbfe41c42a3f08e9cb4",
  "0b7f849446d3383546d15a480966084442cd2193",
  "06e6eef6adf2e5f54ea6c43c376d6d36605f810e",
  "7d112681b8dd80723871a87ff506286613fa9cf6",
  "312a46dc52117efa4e3096eda510370f01c83b27",
  "7bdeecc97cf8f9b9188ba2751aa1755dad9ff819",
  "a14c955bda572b817deccc3a2135cc5f2518c1d3",
  "86f28434210631fa6bda6db990aba7391f512774",
  "d089da97b9e447158a0466d15fe291f2c43b982e",
  "3d5cbfed48ce23d2f0dc0a0baa3ec2ee93867b2b",
  "e45ed40f34005e1636649ab18bbd16ada02cb251",
  "d6fa2beb1c302491b40f447d8784fc0bcce1ca8e",
  "be17881e010a71c3fa3f4e9650242341c764b39a",
  "5de2a2a23e0b3beee08b75a6b0c0cd3847f0d7be",
  "bbe2aeb4e25b2b007eb4b63d59bdf4ad6be2378b",
  "d47e69ada060f488a539a1383dac8275b76d9dd5",
  "5484904228e84abd75e235c359d3dcffc222583c",
  "95bb0330ffec243b47d3916e7ccf507e27fd5c2d",
  "6571420001ed3ab44b515e25ccd5877195a5da6b",
  "163314f09e8aaf4472274c78bd520d915a7a4711",
  "1ded2e3c92caf7b7082534acbd3aa63ba729d53f",
  "7b11d0e5c4e8a2a089111d86b36a2ffa8409e2e2",
  "18727a62524792efc6f1d6c77825901e00bbcc26",
  "8b7c6961ad485cbd1bb703cc716778ffba5796d3",
  "231beaba6c347708daf2d19e5f257a1557114d11",
  "4126622a7e5047020c093ee88e686d7107e68476",
  "2985f7a35e87e4ab65e12e2089a4d5a0ca7a95b1",
  "b65dd2ee27376a9d4d6ab4d038424ea15c9cb92b",
  "e9ffd54866e79ef837b8f376c44e7325d65f6969",
  "075146c5e014723717539ed022b8515cd4903be2",
  "f3c41043ce753ede3017f8b21dbac8d34af8eb7d",
  "440689149348083e442edbc25f3227650c346c27",
  "48f4e1d4d2d201b0608cf032c48066842135aa6c",
  "8cb8a308c404a31e3bac28fb29c79e3894e7d965",
  "95ac1c4f9ef194240ee5e40ea6c4f50f9285b08e",
  "5753c716035f48ce59d56c66d0ba03984ee017d8",
  "cb3b0cce14ebf6bdf2f045be1477d39c60285532",
  "4b059de04dc00b947648cf75b4fdcccb9f6ad880",
  "2d2e938d65234672e2a66f88c2bc1eccafa8ffad",
  "f2401778850e52d64a6bc3b51886f6983c44d810",
  "fa2d1c9a22c9fbbe8e357a37d15e2505cb47733a",
  "4b2f84529047da089ff00aec0e6bd3bf2e8e6d92",
  "3a289e987993f35bd56100cf693d879491710bbf",
  "3e86f0b5ee42d9e8ac874a82c89733ef9b1fb9c1",
  "c0c973ff32ec66865f4ca5ea4792f083e3db86ba",
  "f9fea5da153cfbb54021cdb5bc1a2d1ee9d9e710",
  "25396d3ae6192ed05d588c5356016479b12ffb2f",
  "18c078f39d42ccad5e6983899612c67d150ad8d8",
  "dc1ed9f43eb025bd2d161ca69877ef9ded793471",
  "e00cd671b88e5a475964c73597e2e1fa18506847",
  "f1f74a6a5e58fd07302ecccf27582e102729ca6f",
  "9456aa70855f9c328bc8f462e1c0393a7a8b7ed5",
  "096335f4933bd419029961cb391cf3d250853f00",
  "17569bed06956c36ed067c32380c82d25a82e2a2",
  "edaf30fe2718d273da043562ef22b7f0954b64e7",
  "38073b033a1811e6c37497c0408734e00ea22853",
  "0151b09d30fece47a252d95e609fb21bb6d1d6bf",
  "d529e587642d4c61720531526aa44c299acfbbf9",
  "069f27ccdb9dab4b93aa6cc8a6b34fd429015a4b",
  "123eb19eb2d06df14a43edf0cc9371d5065d1828",
  "ae62edeb88f78489d5836236d1037f6cd481675c",
  "4de7935cf3a38ab6d94fe14048103977dfb2651e",
  "0ab402f20814d5ded5e70926ee11f5bcc070b196",
  "14cdbb5b1c6529f8985a0dfdb4009ae796e4fbea",
  "80d11b8d3c22ce7e0ebc064ab71d9a2de48478e6",
  "170672d3e88f930f725d03fac4f23f56050d024f",
  "84bac8425efa9a77bc552220f5af126c1d41afe2",
  "13d5f43726da853c07caf78371d9106c4c27d1d2",
  "988b8b7647ee7aba87deb60391ea8173f4389f83",
  "25f6a9c9380180bf7faed131ef8b5ab7784d0e0b"
}

stores_table = {
  "NYC", "MSK", "SPB", "TVT", "EKB"
}

names_table = { "Fedor", "Sasha", "Katya", "Bella", "Olivia", "Mannie" }

phones_table = { "89991233", "+89991212", "+89777233", "+89922223", "89991444", "89444233" }

requests_table = {
  [[{
    "user_hash": "]].. users_table[ math.random( #users_table ) ] ..[[",
    "products": [
      {
        "title": "MacBook Pro 15",
        "description": "No need to describe",
        "cost": 4000,
        "quantity": 1,
        "storehouse":    "]].. stores_table[ math.random( #stores_table ) ] ..[["
      }
    ],
    "shipping_cost": 0.0,
    "paid_amount": 0.0,
    "contact_name":  "]].. names_table[ math.random( #names_table ) ]   ..[[",
    "contact_phone": "]].. phones_table[ math.random( #phones_table ) ] ..[[",
    "destination": "Šetalište Iva Vizina br., 18 \n Tivat, Montenegro, 85777"
  }]],
  -- Some with fractions
  -- [[{
  --   "user_hash": "]].. users_table[ math.random( #users_table ) ] ..[[",
  --   "products": [
  --     {
  --       "title": "DELL XPS 13",
  --       "description": "No need to describe",
  --       "cost": 3500,
  --       "quantity": 3,
  --       "storehouse":    "]].. stores_table[ math.random( #stores_table ) ] ..[["
  --     },
  --     {
  --       "title": "DELL XPS 15",
  --       "description": "No need to describe",
  --       "cost": 1410.50,
  --       "quantity": 1,
  --       "storehouse":    "]].. stores_table[ math.random( #stores_table ) ] ..[["
  --     }
  --   ],
  --   "shipping_cost": 89.50,
  --   "paid_amount": 0.0,
  --   "contact_name":  "]].. names_table[ math.random( #names_table ) ]   ..[[",
  --   "contact_phone": "]].. phones_table[ math.random( #phones_table ) ] ..[[",
  --   "destination": "Šetalište Iva Vizina br., 18 \n Tivat, Montenegro, 85777"
  -- }]],
  [[{
    "user_hash": "]].. users_table[ math.random( #users_table ) ] ..[[",
    "products": [
      {
        "title": "MacBook Pro 13 2015",
        "description": "No need to describe",
        "cost": 2900,
        "quantity": 1,
        "storehouse":    "]].. stores_table[ math.random( #stores_table ) ] ..[["
      },
      {
        "title": "Xiaomi Notebook Pro",
        "description": "No need to describe",
        "cost": 1000,
        "quantity": 5,
        "storehouse":    "]].. stores_table[ math.random( #stores_table ) ] ..[["
      }
    ],
    "shipping_cost": 100.00,
    "paid_amount": 0.0,
    "contact_name":  "]].. names_table[ math.random( #names_table ) ]   ..[[",
    "contact_phone": "]].. phones_table[ math.random( #phones_table ) ] ..[[",
    "destination": "Šetalište Iva Vizina br., 8 \n Tivat, Montenegro, 85320"
  }]],
  [[{
    "user_hash": "]].. users_table[ math.random( #users_table ) ] ..[[",
    "products": [
      {
        "title": "MacBook Pro 13",
        "description": "No need to describe",
        "cost": 3800,
        "quantity": 10,
        "storehouse":    "]].. stores_table[ math.random( #stores_table ) ] ..[["
      }
    ],
    "shipping_cost": 2000.00,
    "paid_amount": 0.0,
    "contact_name":  "]].. names_table[ math.random( #names_table ) ]   ..[[",
    "contact_phone": "]].. phones_table[ math.random( #phones_table ) ] ..[[",
    "destination": "Šetalište Iva Vizina br., 12 \n Tivat, Montenegro, 83337"
  }]],
}

request = function()
  socket.sleep(1e-8)
  math.randomseed( tonumber(tostring(socket.gettime() * 10000):reverse():sub(1,6)) )

  wrk.headers["Content-Type"] = "application/json"
  return wrk.format("POST", path,  headers, requests_table[ math.random( #requests_table ) ])
end
