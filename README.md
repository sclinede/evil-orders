# Evil Orders

Это микросервис для офоромления и отправки товаров со складов одного маленького, но гордого и международного магазина для продажи ноутбуков.

Пользователь может заказать до N ноутбуков из N имеющихся в наличии и оплатить их. Ноутбуки могут быть на разных складах и их необходимо отправить пользователю минимальным количеством посылок (по одной с каждого склада).

В основном вся работа осуществляется через API, но есть и дашборд, куда выводится статистика для человеков.


## Запуск

Вам понадобятся: Ruby, PostgreSQL и Redis не сильно древних версий.

Создайте файл `.env` с содержимым вида:

```sh
DATABASE_URL=postgres:///evil_orders_development
DATABASE_POOL=16
REDIS_URL=redis://localhost:6379/10
```

Заполните БД:

```sh
bundle exec rake db:setup
```


Запустите приложение:

```sh
rackup
```

Откройте в браузере: http://localhost:9292/


## Устройство приложения

Для удобства отладки (чтобы не скакать между файлами) всё Rails-приложение находится в одном файле [`app.rb`](app.rb).

Порядок: конфигурация → контроллеры → модели → сервис-объекты

Приложение полностью готово для деплоя на Heroku.


## Что-то тут не так…

Пользователи жалуются, что дашборд очень медленно загружается, а ещё у нас ужасающе огромное потребление памяти для такого небольшого микросервиса.

Что будем делать?

Обратите внимание, что тестов в приложении нет, но подразумевается, что они есть и они все проходят. А проблемы-то всё равно есть…
