# frozen_string_literal: true

# rubocop:disable all

require_relative "./app"

run Rails.application
