ActiveRecord::Schema.define(version: 20171201120000) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "orders", force: :cascade do |t|
    # а не забыли про лимиты?
    t.string  "user_hash", limit: 40
    t.string  "products", array: true
    t.integer "products_cost", null: false, default: 0
    t.integer "shipping_cost", null: false, default: 0
    t.integer "paid_amount", null: false, default: 0
    t.string  "contact_name", null: false
    t.string  "contact_phone", null: false
    t.string  "payment_status"
    t.jsonb   "raw", default: {}
    t.timestamps
  end

  create_table "parcels", force: :cascade do |t|
    t.string   "user_hash", limit: 40
    t.string   "name", null: false
    t.integer  "shipping_cost"
    t.string   "origin"
    t.string   "destination"
    t.string   "shipped_by"
    t.string   "tracking_number"
    # а не забыли про лимиты?
    t.string   "products", array: true, default: []
    t.datetime "shipped_at"
    t.datetime "delivered_at"
    t.string   "status"
    t.timestamps
  end

  create_table "products", force: :cascade do |t|
    t.string "title", null: false
    t.string "description"
    t.integer "cost"
    t.integer "quantity"
    t.string  "storehouse", null: false
    t.references :parcel
    t.references :order
    t.timestamps
  end
end
