require "faker"
require_relative "../instrumenter"

def with_profile
  return yield unless ENV.fetch("profile", false)

  case ENV["profile"]
  when "instrument"
    name = ENV.fetch("name", "db:seed")
    instrument = Obiq::Instrumenter.new("Seeds Load")
    instrument.measure(name, memory: true) { yield }
    instrument.dump
  when "rubyprof"
    result = RubyProf.profile { yield }

    printer = RubyProf::FlatPrinter.new(result)
    printer.print File.new('tmp/rubyprof_flat_profile', 'w+')

    printer = RubyProf::GraphPrinter.new result
    printer.print File.new('tmp/rubyprof_graph_profile', 'w+')

    printer = RubyProf::GraphHtmlPrinter.new result
    printer.print File.new('tmp/rubyprof_graph_html_profile.html', 'w+')

    printer = RubyProf::CallStackPrinter.new result
    printer.print File.new('tmp/rubyprof_call_stack_profile.html', 'w+')
  when "stackprof"
    # wall vs object
    mode     = ENV.fetch("mode").to_sym
    interval = ENV.fetch("interval", 100).to_i
    repeat   = ENV.fetch("repeat", 2).to_i
    dump     = "tmp/#{mode}_#{repeat}_#{interval}"

    StackProf.run mode: mode, out: dump, interval: interval do
      repeat.times { yield }
    end
  else
    raise ArgumentError, "unknown profiler"
  end
end

if ENV["only_payments"]
  Order.where.not(payment_status: :success)
       .order('random()')
       .limit(5)
       .each do |order|
    PaymentProcessor.call(
      order,
      amount: order.total_cost,
      currency: "USD"
    )
  end

  exit(0)
end

users = []
seed_users = ENV["users"] || 5

seed_users.to_i.times do
  users << Digest::SHA1.hexdigest(Faker::Team.name)
end

with_profile do
  seed_orders = ENV["orders"] || 100
  seed_orders.to_i.times do
    request = {
      user_hash: users.sample,
      products: [
        {
          title: Faker::Food.dish,
          description: Faker::Movie.quote,
          cost: 2.1,
          quantity: 1,
          storehouse: Product::STOREHOUSES.keys.sample
        },
        {
          title: Faker::Food.dish,
          description: Faker::Movie.quote,
          cost: 10,
          quantity: 1,
          storehouse: Product::STOREHOUSES.keys.sample
        }
      ],
      currency: PaymentProcessor::CURRENCY_RATES.keys.sample,
      shipping_cost: 1.0,
      paid_amount: 0.0,
      contact_name: Faker::Team.name,
      contact_phone: Faker::PhoneNumber.cell_phone,
      destination: "Šetalište Iva Vizina br., 8 \n Tivat, Montenegro, 85320"
    }

      order = OrdersCreator.call(request)
      ParcelsCreator.call(order, request)
  end
end

puts "Orders Seeded. Count: #{Order.count}"
puts "Parcels Seeded. Count: #{Parcel.count}"
puts "Products Seeded. Count: #{Product.count}"
