module CacheInstrumentation
  thread_mattr_accessor :should_measure
  thread_mattr_accessor :hit_or_miss

  ::ActiveSupport::Notifications.subscribe "start_processing.action_controller" do |*args|
    data = args.extract_options!
    self.should_measure = data[:controller] == "HomeController" && data[:action] == "index"
  end

  ::ActiveSupport::Notifications.subscribe "cache_read.active_support" do |*args|
    next unless self.should_measure

    data = args.extract_options!
    # next unless data[:key] =~ /.../
    self.hit_or_miss = data[:hit] ? 'HIT' : 'MISS'
  end

  ::ActiveSupport::Notifications.subscribe "process_action.action_controller" do |*args|
    next unless self.should_measure

    event = ActiveSupport::Notifications::Event.new(*args)
    Rails.logger.info "Cache #{self.hit_or_miss} (#{event.duration} ms)"
  end
end

# Если какой-то код делает запросы в базу, но вы не знаете какой именно
# вам подойдет такой вариант инструментации.
# Однако, если проблема происходит на этапе запуска приложения вам нужно
# убедиться что вы подписываетесь на нотфикакии сразу после инициализации
# Рельс, но ДО инициализации прочего кода.
::ActiveSupport::Notifications.subscribe "sql.active_record" do |*args|
  # data = args.extract_options!
  # next unless data[:sql] =~ /FROM "orders"/
  #
  # # next unless data[:name] =~ /.../
  # # next unless data[:binds] =~ /.../
  #
  # Rails.logger.warn <<~TEXT
  #   [PG_CONN=#{data[:connection_id]}]
  #   sql=#{data[:sql]}
  #   app_stack=#{caller.join('; ')}"
  # TEXT
end
