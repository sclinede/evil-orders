require "oink/instrumentation/memory_snapshot"

module Obiq
  class Instrumenter
    IGNORED_QUERIES_PATTERN = %r{(
      pg_table|
      pg_attribute|
      pg_namespace|
      show\stables|
      \ATRUNCATE TABLE|
      \AALTER TABLE|
      \ABEGIN|
      \ACOMMIT|
      \AROLLBACK|
      \ARELEASE|
      \ASAVEPOINT
    )}xi

    class << self
      def logger
        @logger ||= ActiveSupport::TaggedLogging.new(Logger.new(STDOUT))
      end

      def memsize
        memory_snapshoter.memory
      end

      def memory_snapshoter
        @memory_snapshoter ||=
          Oink::Instrumentation::MemorySnapshot.memory_snapshot_class.new
      end
    end

    def initialize(target, db_profile: false)
      @target = target
      setup_db_profiling if db_profile
      reset
      label("created_at", Time.now)
    end

    def reset
      @data = Hash.new { |h, k| h[k] = 0.0 }
    end

    def setup_db_profiling
      ::ActiveSupport::Notifications.subscribe("sql.active_record") do |_name, start, finish, _id, query|
        next if query[:sql] =~ IGNORED_QUERIES_PATTERN
        collect("db_time", finish - start)
        collect("db_count")
      end
    end

    def measure(name, memory: false)
      start = Time.now.to_f

      if memory
        mem_start = self.class.memsize
        collect("#{name}_memsize_start", mem_start)
      end

      res = nil
      begin
        res = yield
      ensure
        collect(name, Time.now.to_f - start)
        if memory
          mem_finish = self.class.memsize
          collect("#{name}_memsize_finish", mem_finish)
          collect("#{name}_memsize_delta", mem_finish - mem_start)
        end
      end
      res
    end

    def collect(name, value = 1)
      @data[name] += value
    end

    def label(name, value)
      @data[name] = value
    end

    def dump
      logger.tagged("OBIQ INSTRUMENT #{@target}") { logger.info @data.to_json }
    end

    private

    def logger
      self.class.logger
    end
  end
end
